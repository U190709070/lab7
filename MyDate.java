import jdk.jshell.execution.JdiExecutionControl;

public class MyDate {
    int year;
    int month;
    int day;

    public MyDate(int day, int month, int year) {
        this.year = year;
        this.month = month;
        this.day = day;
    }
    public MyDate(MyDate date){
        this(date.day, date.month, date.year);
    }

    @Override
    public String toString() {
        if (this.month < 10){
            return this.year + "-0"+ this.month + "-" + this.day;
        }
        return this.year + "-"+ this.month + "-" + this.day;
    }
    public boolean isBefore(MyDate date){
        if (this.year > date.year){
            return false;
        }else if (this.month > date.month){
            return false;
        }else if (this.day > date.day){
            return false;
        }else {
            return true;
        }
    }
    public boolean isAfter(MyDate date){
        if (this.year > date.year){
            return true;
        }else if (this.month > date.month){
            return true;
        }else if (this.day > date.day){
            return true;
        }else {
            return false;
        }
    }
    public int dayDifference(MyDate date){
        if (this.year != date.year) {
            int y = Math.abs((this.year - date.year) * 360);
            int m = Math.abs((this.month - date.month) * 31);
            int d = Math.abs((this.day - date.day));
            return y - m - d;
        }else if (this.month != date.month){
            int m = Math.abs((this.month - date.month) * 31);
            int d = Math.abs((this.day - date.day));
            return m - d;
        }else{
            int d = Math.abs((this.day - date.day));
            return d ;
        }
    }
    public void incrementYear(){
        this.year++;
    }
    public void incrementYear(int x){
        this.year = this.year + x;
    }
    public void decrementYear(){
        this.year--;
    }
    public void decrementYear(int x){
        this.year =  this.year - x;
    }
    public void incrementMonth(){
        if (this.month == 12){
            this.month = 1;
            incrementYear();
        }else{
            this.month++;
        }

    }
    public void incrementMonth(int x){
        if (x > 12) {
            int num = Math.floorDiv(x, 12);
            incrementYear(num);
            int diffnum = x % 12;
            this.month += diffnum;
            if (this.month > 12){
                this.month = this.month - 12;
                incrementYear();
            }
        }else {
            this.month = this.month + x;
            if (this.month > 12){
                this.month = this.month - 12;
                incrementYear();
            }
        }

    }
    public void decrementMonth(){
        if (this.month == 1){
            this.month = 12;
            decrementYear();
        }else{
            this.month--;
        }
    }
    public void decrementMonth(int x){
        if (x > 12) {
            int num = Math.floorDiv(x, 12);
            decrementYear(num);
            int diffnum = x % 12;
            this.month -= diffnum;
            if (this.month < 0){
                this.month = this.month + 12;
                decrementYear();
            }

        }else {
            this.month = this.month - x;
            if (this.month < 0){
                this.month = this.month + 12;
                decrementYear();
                }
        }
    }
    public void incrementDay(int x){
        if (x > 31){
            int num = Math.floorDiv(x, 31);
            incrementMonth(num);
            int diffnum = x % 31;
            this.day += diffnum;
            if (this.day > 31){
                this.day -= 31;
                incrementMonth();
            }
        }else {
            this.day = this.day + x;
            if (this.day > 31){
                this.day -= 31;
                incrementMonth();
            }
        }
    }
    public void decrementDay(int x){
        if (x > 31){
            int num = Math.floorDiv(x, 31);
            decrementMonth(num);
            int diffnum = x % 31;
            this.day -= diffnum;
            if (this.day < 0){
                this.day += 31;
                decrementMonth();
            }
        }else {
            this.day -= x;
            if (this.day < 0){
                this.day += 31;
                incrementMonth();
            }
        }
    }
    public void incrementDay(){
        if (this.month == 2){
            if (this.day == 28){
                this.day = 1;
                incrementMonth();
            }else {
                this.day++;
            }
        }else {
            if (this.day == 31){
                this.day = 1;
                incrementMonth();
            }else {
                this.day++;
            }
        }
    }
    public void decrementDay(){
        if (this.month == 3){
            if (this.day == 1){
                this.day = 28;
                decrementMonth();
            }else{
                this.day--;
            }
        }else{
            if (this.day == 1){
                this.day = 31;
                decrementMonth();
            }else {
                this.day--;
            }
        }
    }
}